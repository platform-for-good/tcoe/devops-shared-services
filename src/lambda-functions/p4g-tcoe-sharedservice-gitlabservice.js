const gitlab = require('node-gitlab');
const axios = require('axios');

var client = gitlab.createPromise({
  api: 'https://gitlab.com/api/v4',
  privateToken: 'cTLaijkGhyedrxpD9V2R'
});

var response = {
  statusCode: 500,
  body: JSON.stringify('Invalid Operation')
};

exports.handler = async (event) => {
  // TODO implement
  var operation = event.operation;
  switch (operation) {

    case 'users.get':
      var gitlabusername = event.gitlabusername;
      var gitlabGetUserURL = 'https://gitlab.com/api/v4/users?username=' + gitlabusername;
      const response = await axios.get(gitlabGetUserURL);
      const result = response.data;
      return result;

    case 'groups.list':
      let groups = await client.groups.list();
      for (var i = 0; i < groups.length; i++)
        console.log(groups);
      return groups;

    case 'groups.create':
      var groupName = event.name;
      var groupPath = groupName.toLowerCase();
      groupPath = groupPath.replace(/[^a-zA-Z0-9]/g, '-').replace(/--/g, '-');
      var groupDescription = event.description;
      var groupCreatorGitLabUserId = event.creator_gitlabuserid;

      let createdGroup = await client.groups.create({ "name": groupName, "path": groupPath, "parent_id": 8748133, "description": groupDescription, "visibility": "public" });
      let ownerGroupMember = await client.groupMembers.create({ "id": createdGroup.id, "user_id": groupCreatorGitLabUserId, "access_level": 50 });
      console.log(ownerGroupMember);
      return createdGroup;

    case 'groupMembers.create':
      var gitlabgroupId = event.gitlabgroupid;
      var gitlabuserid = event.gitlabuserid;
      var accessLevel = event.access_level;

      let addedGroupMember = await client.groupMembers.create({ "id": gitlabgroupId, "user_id": gitlabuserid, "access_level": accessLevel });
      return addedGroupMember;

    case 'projects.create':
      var gitlabgroupId = event.gitlabgroupId;
      var projectName = event.name;
      //var projectPath= projectName.toLowerCase();
      //projectPath = projectPath.replace(/[^a-zA-Z0-9]/g,'-').replace(/--/g,'-');
      var projectDescription = event.description;
      var projectCreatorGitLabUserId = event.creator_gitlabuserid;

      let createdProject = await client.projects.create({ "name": projectName, "namespace_id": gitlabgroupId, "description": projectDescription, "visibility": "public" });
      let ownerProjectMember = await client.projectMembers.create({ "id": createdProject.id, "user_id": projectCreatorGitLabUserId, "access_level": 50 });
      console.log(createdProject);
      console.log(createdProject.id);
      console.log(projectCreatorGitLabUserId);
      return createdProject;

    case 'projectMembers.create':
      var gitlabprojectId = event.gitlabprojectid;
      var gitlabuserid = event.gitlabuserid;
      var accessLevel = event.access_level;

      let addedProjectMember = await client.projectMembers.create({ "id": gitlabprojectId, "user_id": gitlabuserid, "access_level": accessLevel });
      return addedProjectMember;

    default:
      return response;
  }
};